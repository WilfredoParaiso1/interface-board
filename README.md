# Interface and Power Distribution Board

Created on behalf of University of California, Irvine's Hyperloop Team, [HyperXite](https://hyperxite.com "HyperXite").

Check out the GitHub [here](https://github.com/HyperXite "HyperXite GitHub").

Could not have been created without the help from HyperXite's sponsor, [Altium](https://altium.com).

#### Author: 

    Andres Tec

#### Designers:

    Andres Tec
    Sean McCutcheon
    Langtian Tan

![2Dview](Previews/2D.png "PCB Overview")

![3Dview](Previews/3D.png "PCB Overview")

![Completed Design](Previews/completed_design.png "Manufactured by JLCPCB")

## Purpose and Overview

The PCB was created to simplify electrical debugging and reduce problems related to wiring complications. After encountering many issues during the 2nd design iteration of the Hyperloop Pod, the team needed a PCB to simplify the design for pod design 3. The team also needed to create quick prototypes of the board at a low cost, so [JLCPCB](https://jlcpcb.com) was chosen to be the manufacture.

Read more about the pod design [here](https://www.hyperxite.com/pod/ "Pod Overview"). For a more detailed overview, click [here](https://escholarship.org/uc/item/3gz6m0kc "Pod 3 Poster").

The board had to distribute power and route each sensor's and actuator's signals to the main controller: National Instrument's Compact RIO. It also had to function in near-vacuum conditions, provide power to telemetry components, provide visual indicators for actuation, and have over-current protection (non-instant).


`Before`

![HXII](Previews/hyperxiteii_central_circuit.gif "HyperXite II Pod Wiring Mess")

`After`

![Integration](Previews/pod_integration.png "HyperXite III PCB Integration" )

### Signals

Here is the list of the sensors and actuators needing to be powered and its signals relayed to the various modules placed into the [NI 9063 Compact RIO](http://www.ni.com/en-us/support/model.crio-9063.html). A full detailed list can be viewed in the [BOM](Output%20Job%20Files/BOM_InterfaceBoard.htm "Sensors and Actuators list").

| Component Type | Quantity | Type                                | Purpose                                                           | Part                                                     |
| -------------- | -------- | ----------------------------------- | ----------------------------------------------------------------- | -------------------------------------------------------- |
| Sensor         | 1        | Pressure Transducer (PT)            | Tank Pressure (HP)                                                | PX119-3KGI (3000PSI gage), PX119-1.5KGI (1500PSI gage)   |
| Sensor         | 1        | PT                                  | Pneumatic Muscles (LP)                                            | PX119-150GI                                              |
| Sensor         | 1        | PT                                  | Brakes (LP)                                                       | PX119-150GI                                              |
| Actuator       | 2        | Air Spring                          | Brakes                                                            | McMaster (9538K23)                                       |
| Actuator       | 1        | Pump                                | Pumping from Resevoir                                             | GRI Prumps INTG3-562 EPDM                                |  |
| Sensor         | 2        | Temperature Reader                  | Temp for Fairing, High and Low voltage vessels                    | Surface-Mount RTD Advanced-Design: Stick-On or Cement-On |
| Sensor         | 1        | Ammeter                             | Current output for High and Low voltage                           | Allegro ACS758xCB                                        |
| Actuator       | 1        | Motor Controller (3-Phase Inverter) | Control Motor, Measure Motor behavior                             | Emsiso emDrive H300                                      |
| Sensor         | 1        | Resolver                            | Motor Rotor Position                                              | Provided by EMRAX Motors                                 |
| Sensor         | 1        | Temperature Sensor                  | Measure Rotor Temperature                                         | Provided by EMRAX Motors                                 |
| Sensor         | 1        | Temperature Sensor                  | Measure Stator Temperature                                        | Provided by EMRAX Motors                                 |
| Sensor         | 1        | Fiducial Sensor                     | Count the amount of Red Tape crossed in the tube for Displacement | Balluff BOS 6K-PU-RD10-S49                               |
| Sensor         | 1        | IMU                                 | Pod acceleration, delta velocity, pressure, position              | SBG Ellipse-N-B1                                         |
| Sensor         | 4        | Linear Potentiometer                | Braking Actuation confirmation                                    | Honeywell  75mm S.M.A.R.T. Position Sensor               |
| Actuator       | 3        | Solenoid Valve                      | Control air flow (actuation)                                      | 24 VDC 3-Way Value 23GG9ZCM                              |
| Other          | 1        | High Power Contactor                | Safely Control Use of High DC Voltage                             | TE Connectivity LEV200A4ANA                              |
| Actuator       | 6        | Relay                               | Solenoid Valves, Pump Control                                     | G5LE                                                     |  |  |  |
| Sensor         | 1        | Pressure Sensor                     | Battery Pressure Vessel leak detect                               | ASDXACX030PAAA5                                          |

The signals utilized were mainly analog signals except for the following:

| Name                                                                                                            | Protocol |
| --------------------------------------------------------------------------------------------------------------- | -------- |
| [Orion 2 Battery Management System](https://www.orionbms.com/products/orion-bms-standard/ "Orion 2")            | CANopen  |
| [Emsiso emDrive H300 3-Phase Inverter](https://www.emdrive-mobility.com/emdrive-h300 "emDrive H300")            | CANopen  |
| [NI 9881 CANopen Module (Compact RIO Module)](http://www.ni.com/en-us/support/model.ni-9881.html)               | CANopen  |
| [SBG Ellipse-N-B1 Interial Measurement Unit (obselete)](https://www.sbg-systems.com/products/ellipse-2-series/) | RS232    |

  The CANopen protocol bitrate was set to the standard 500 Kbps rate.

#### Signal Integrity

 The digital signals were routed as a differential pair on the PCB, however no cross-talk analysis was done between nets near the routing. Because of the limited time-frame we had to create the working prototype Hyperloop pod, little signal integrity analysis was done to ensure quality of each net. So, only behavior verification was done during testing of the board.

### Power

The subsystem functioned on 2 power rails: 24 VDC and 5 VDC. The 5 VDC rail was mainly used for the op-amps used to generate an accurate voltage output signal from the RTD sensor (current source). The rest of the system's components, e.g., relays and sensors, were all chosen to function at 24 volts. The system was stepped-down to 5 VDC using Recom's DC/DC Converter, RPA50S-W. This converter is capable of bypassing 50W of power.

#### Power Analysis

The subsystem was relatively low power compared to our high-power system (150 A peak at 450 Vrms). The maximum power expected to distribute was ~24 watts. Have a look of the maximum capability testing [here](Preview/power_testing.m4v "Power Testing").

Again, due to the time constraint little power analysis was done for the low-power system. Due to this, some of our sensor readings shifted during verification testing caused by voltage drops. However, at a minimum the system was galvanically isolated from the high-power system for safety reasons.

## Future Design Recommendations

##### True Embedded System

A fairly large and heavy controller was chosen for the sake of quick prototyping and ease-of-use. LabVIEW also made it easy to create both the front-end and back-end of the software, however if given enough time the team should have went with a more traditionally "embedded" system in order to reduce weight for the sake of maximum top-speed and put in-place either on this PCB or on another.

Initial iterations of the subsystem design looked into the use of Texas Instruments Hercules MCU Arm® Cortex®-R. This seemed like a viable option and also met all of our requirements, however an embedded software engineer was not found to support the decision.

##### Conformal Coating

If the pod were the be in vacuum conditions for long periods of time, conformal coating the board is a must-do option. However, because of consistent debugging of the system up to the day of competition, this was not a current-viable option.

##### Automated Electronics Testing and Verification

Hyperloop is meant to scale. If this subsystem were to be mass-produced, various test-points and test-pins need to be seriously considered and incorporated into this design. Some test points were implemented, but are only accessible from the top of the PCB. Routing needs to be done to designated and accessible pins.

##### Cost-Reduciton

Components like the Recom RPA50S-W DC/DC Converter is relatively low-cost for the family of DC/DC converters capable of handling 50W of power, however the component costs nearly as much as the PCB board it was placed onto. If we closely analyzed our power requirements we could have chosen a much more suitable part for our design to reduce the subsystem's overall cost.

##### More Safety

To abide to automotive and industrial standards, like ISO 26262-1, minumum safety requirements like **over-voltage** and (instant) **over-current** protection circuits need to be implemented into the product. Fold-back circuits and Crowbar circuits could have easily be designed into the subsystem to provide these features.